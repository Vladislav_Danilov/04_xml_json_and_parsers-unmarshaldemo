package productsInShopRun;

import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.stream.StreamSource;

import productsInShopJAXB.Category;
import productsInShopJAXB.Products;

public class Main {

  public static void main(String[] args) {
    XMLInputFactory xif = XMLInputFactory.newFactory();
    StreamSource xml = new StreamSource("xmlSrc/ProductInShopXML.xml");
    JAXBElement<Products> jb = null;
    XMLStreamReader xsr;
    try {
      xsr = xif.createXMLStreamReader(xml);
      xsr.nextTag();
      while (!xsr.getLocalName().equals("products")) {
        xsr.nextTag();
      }

      JAXBContext jc = JAXBContext.newInstance(Products.class);
      Unmarshaller unmarshaller = jc.createUnmarshaller();
      jb = unmarshaller.unmarshal(xsr, Products.class);
      xsr.close();
    } catch (XMLStreamException e) {
      e.printStackTrace();
    } catch (JAXBException e) {
      e.printStackTrace();
    }
    Products products = jb.getValue();
    List<Category> category = products.getCategory();
    category.stream().forEach(c -> {
      System.out.println("category=" + c.getNameCategory());
      c.getSubcategory().stream().forEach(sc -> {
        System.out.println("subcategory=" + sc.getNameCategory());
        sc.getProduct().stream().forEach(p -> {
          System.out.format(
              "name=%s; date=%s model=%s; color=%s price=%.2f; quantity in stock=%s \n",
              p.getNameProduct(), p.getDate(), p.getModel(), p.getColor(), p.getPrice(),
              p.getQuantity());
        });
      });
    });
  }

}
